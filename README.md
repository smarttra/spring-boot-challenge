# TTMzero Spring Boot Challenge #

## Before starting ##
1. Make shure,[sbt]( https://scala-sbt.org/) is installed -- any recent version will do.
2. Also, Docker and docker-compose must be available.

## Getting started ##
1. `git clone ...`
2. `cd spring-boot-challenge`
3. `sbt docker:stage && docker-compose up --build`

This should get you some running app.

## Challenges ##
Implement a service which communicates via kafka by producing and consuming messages.
The following features should be implemented:

1. Consume messages from the _insert_ topic and persist its content
2. Consume messages from the _update_ topic and update an existing entity
3. After consumption, the service should produce a message that contains the updated state of the entity.

This is the JSON mmodel of the entity to persist:
```
{
  "product_id" : "FX_Res_Knock_Into_FW_Imp_eu",
  "quanto" : true,
  "creationDate" : "2018-05-25",
  "expirationDate" : "2020-05-25",
  "terms" : {
    "events" : [ {
      "type" : "EXECUTION",
      "terminal" : true,
      "execution" : {
        "on" : {
          "kind" : "SCHEDULE"
        },
        "origin" : "EUROPEAN",
        "type" : "EXECUTION"
      }
    }]
    }
}
```

## Hints ##
1. For persistence, any kind of in-memory storage will do. Keep it simple.
   But be aware, that messages might be consumed in different threads.
2. You may use the project from this repository as a starting point.
   But this is optional. If you do so, it might be a good idea to create a fork.
3. If questions arise, try to guess a reasonable answer. Ideally, document it.
   Otherwise, you can always contact us to ask questions.
4. Writing tests here is not a strict requirement -- but it surely doesn't harm

