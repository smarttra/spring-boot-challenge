import com.typesafe.sbt.packager.docker._

Global / onChangedBuildSource     :=  ReloadOnSourceChanges

ThisBuild / organization          :=  "ttmzero"
ThisBuild / name                  :=  "spring-boot-task"

ThisBuild / crossPaths            :=  false
ThisBuild / autoScalaLibrary      :=  false
Compile / compile / javacOptions ++=  Seq("-source", "13", "-target", "13", "-g:lines")

ThisBuild / resolvers             +=  Resolver.jcenterRepo

libraryDependencies ++= Seq("org.springframework.boot"    % "spring-boot-starter"     % "2.2.4.RELEASE",  
  "org.springframework.kafka"   % "spring-kafka"            % "2.4.1.RELEASE",
  "org.junit.jupiter"           % "junit-jupiter-engine"    % "5.0.0-M3"                        % "test",
  "net.aichler"                 % "jupiter-interface"       % JupiterKeys.jupiterVersion.value  % "test")

enablePlugins(DockerPlugin, JavaAppPackaging)

Docker / packageName              :=  "docker.ttmzero.com/tasks/spring-boot"
Docker / executableScriptName     :=  "fiap-api-gateway"
dockerBaseImage                   :=  "openjdk:13"
