resolvers += Resolver.jcenterRepo

addSbtPlugin("net.aichler" % "sbt-jupiter-interface" % "0.8.3")
addSbtPlugin("com.typesafe.sbt" % "sbt-native-packager" % "1.5.1")
